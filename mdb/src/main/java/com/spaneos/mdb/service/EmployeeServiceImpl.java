package com.spaneos.mdb.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.mongodb.WriteResult;
import com.spaneos.mdb.domain.Employee;
import com.spaneos.mdb.domain.EmployeeDTO;
import com.spaneos.mdb.domain.Status;
import com.spaneos.mdb.repo.EmployeeRepo;
import com.spaneos.mdb.service.exception.ConcurrentModificationException;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepo employeeRepo;
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public Employee jobOfferedTo(EmployeeDTO employeeDTO) {

		Query query = new Query().addCriteria(Criteria.where("email").is(employeeDTO.getEmail()));
		Update update = new Update().inc("version", 0);
		WriteResult x = mongoTemplate.upsert(query, update, Employee.class);
		if (x.isUpdateOfExisting()) {
			throw new IllegalArgumentException("Person by emai " + employeeDTO.getEmail() + " already exists ");
		}
		Employee employee = mongoTemplate.findAndModify(query,
				new Update().set("firstName", employeeDTO.getFirstName()).set("lastName", employeeDTO.getLastName())
						.set("mobile", employeeDTO.getMobile()).set("status", Status.YETTOJOIN)
						.set("createdBy", employeeDTO.getModifiedBy()).set("modifiedBy", employeeDTO.getModifiedBy())
						.set("createdTime", LocalDateTime.now()).set("modifiedTime", LocalDateTime.now()).inc("version",
								1),
				FindAndModifyOptions.options().returnNew(true).upsert(false), Employee.class);
		return employee;
	}

	@Override
	public Employee jobOfferAcceptedBy(String email, String modifiedBy) throws ConcurrentModificationException {
		Employee employee = employeeRepo.findByEmail(email);
		if (employee == null) {
			throw new IllegalArgumentException(
					"Employee by given email :" + email + " not available in the system. Please check...");
		}
		if (employee.getStatus() != Status.YETTOJOIN) {
			throw new IllegalArgumentException("Employee by given email :" + email + " is not an offered employee...");
		}
		updateStatus(employee, Status.WORKING, modifiedBy);

		return employee;
	}

	@Override
	public Employee employeeResigned(String email, String modifiedBy) throws ConcurrentModificationException {
		Employee employee = employeeRepo.findByEmail(email);
		if (employee == null) {
			throw new IllegalArgumentException(
					"Employee by given email :" + email + " not available in the system. Please check...");
		}
		if (employee.getStatus() != Status.WORKING) {
			throw new IllegalArgumentException("Employee by given email :" + email + " is not working employee...");
		}
		updateStatus(employee, Status.LEFTORG, modifiedBy);
		return employee;
	}

	@Override
	public Employee getEmployeeByEmail(String email) {
		return employeeRepo.findByEmail(email);
	}

	@Override
	public List<Employee> getAllEmployees() {
		return employeeRepo.findAll();
	}

	private void updateStatus(Employee employee, Status status, String modifiedBy)
			throws ConcurrentModificationException {
		employee.setStatus(status);
		employee.setModifiedBy(modifiedBy);
		employee.setModifiedTime(LocalDateTime.now());
		try {
			employee = employeeRepo.save(employee);
		} catch (OptimisticLockingFailureException e) {
			throw new ConcurrentModificationException(
					"Employee with email " + employee.getEmail() + " is being updated by another user");
		}
	}

}
