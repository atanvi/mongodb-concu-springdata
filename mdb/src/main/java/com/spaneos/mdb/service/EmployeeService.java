package com.spaneos.mdb.service;

import java.util.List;

import com.spaneos.mdb.domain.Employee;
import com.spaneos.mdb.domain.EmployeeDTO;
import com.spaneos.mdb.service.exception.ConcurrentModificationException;

public interface EmployeeService {

	public Employee jobOfferedTo(EmployeeDTO employee);

	public Employee jobOfferAcceptedBy(String email,String modifyingUserName)throws ConcurrentModificationException;

	public Employee employeeResigned(String email,String modifyingUserName)throws ConcurrentModificationException;

	public Employee getEmployeeByEmail(String email);

	public List<Employee> getAllEmployees();
}
