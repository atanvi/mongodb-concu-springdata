package com.spaneos.mdb.service.exception;

public class ConcurrentModificationException extends Exception {
	private static final long serialVersionUID = -5508814606348865728L;

	public ConcurrentModificationException(String string) {
		super(string);
	}

}
