package com.spaneos.mdb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.spaneos.mdb.domain.EmployeeDTO;
import com.spaneos.mdb.service.EmployeeService;

@SpringBootApplication
public class MdbApplication {
	@Autowired
	private EmployeeService employeeService;
	@Autowired
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;

	public static void main(String[] args) {
		SpringApplication.run(MdbApplication.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner() {
		return (String... args) -> {
			EmployeeDTO employeeDTO = new EmployeeDTO();
			employeeDTO.setFirstName("Swathi");
			employeeDTO.setLastName("A");
			employeeDTO.setEmail("swathi.a@spaneos.com");
			employeeDTO.setMobile("9886885552");
			
			threadPoolTaskExecutor.execute(() -> {
				try {
					// employeeService.employeeResigned("lakshman.a@spaneos.com",
					// "Laxmi AK");
					employeeDTO.setModifiedBy("Laxmi AK");
					employeeService.jobOfferedTo(employeeDTO);

				} catch (Exception e) {
					e.printStackTrace();
				}
			});
			threadPoolTaskExecutor.execute(() -> {
				try {
					// employeeService.employeeResigned("lakshman.a@spaneos.com",
					// "Pradeep KM");
					employeeDTO.setModifiedBy("Pradeep KM");
					employeeService.jobOfferedTo(employeeDTO);
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
		};
	}

	@Bean
	public ThreadPoolTaskExecutor threadPoolTaskExecutor() {
		ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
		threadPoolTaskExecutor.setCorePoolSize(5);
		threadPoolTaskExecutor.setMaxPoolSize(5);
		threadPoolTaskExecutor.setWaitForTasksToCompleteOnShutdown(true);
		return threadPoolTaskExecutor;
	}

}
