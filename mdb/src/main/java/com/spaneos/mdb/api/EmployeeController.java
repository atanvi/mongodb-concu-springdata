package com.spaneos.mdb.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spaneos.mdb.domain.Employee;
import com.spaneos.mdb.domain.EmployeeDTO;
import com.spaneos.mdb.service.EmployeeService;
import com.spaneos.mdb.service.exception.ConcurrentModificationException;

@RestController
@RequestMapping("/api/employee/")
public class EmployeeController {
	@Autowired
	private EmployeeService employeeService;

	@PostMapping("joboffer")
	public Employee jobOfferedTo(@RequestBody EmployeeDTO employeeDTO) {
		return employeeService.jobOfferedTo(employeeDTO);
	}

	@PostMapping("jobaccept/{email}/{userName}")
	public Employee jobOfferAcceptedBy(@PathVariable("email") String email, @PathVariable("userName") String userName)
			throws ConcurrentModificationException {
		return employeeService.jobOfferAcceptedBy(email, userName);
	}
	@PostMapping("jobresign/{email}/{userName}")
	public Employee employeeResigned(@PathVariable("email") String email, @PathVariable("userName") String userName)
			throws ConcurrentModificationException {
		return employeeService.employeeResigned(email, userName);
	}


	@GetMapping
	public List<Employee> viewEmployees() {
		return employeeService.getAllEmployees();
	}
}
