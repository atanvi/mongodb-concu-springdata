package com.spaneos.mdb.api;

import java.util.Optional;

import org.springframework.hateoas.VndErrors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.spaneos.mdb.service.exception.ConcurrentModificationException;

@ControllerAdvice
public class ExceptionAdvisor {

	@ExceptionHandler(ConcurrentModificationException.class)
	public ResponseEntity<VndErrors> notFoundException(final ConcurrentModificationException e) {
		return error(e, HttpStatus.BAD_REQUEST, e.getMessage());
	}

	@ExceptionHandler(IllegalArgumentException.class)
	public ResponseEntity<VndErrors> assertionException(final IllegalArgumentException e) {
		return error(e, HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
	}

	private ResponseEntity<VndErrors> error(final Exception exception, final HttpStatus httpStatus,
			final String logRef) {
		final String message = Optional.of(exception.getMessage()).orElse(exception.getClass().getSimpleName());
		return new ResponseEntity<>(new VndErrors(logRef, message), httpStatus);
	}
}
