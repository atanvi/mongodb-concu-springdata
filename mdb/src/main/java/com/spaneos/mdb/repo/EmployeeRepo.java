package com.spaneos.mdb.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.spaneos.mdb.domain.Employee;

public interface EmployeeRepo extends MongoRepository<Employee,String>  {

	Employee findByEmail(String email);

}
